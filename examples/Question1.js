/*
1. Does the await keyword mean that the program will wait until the promise is
   settled before continuing to the next line?

The keyword await makes JavaScript wait until that promise settles and returns its result.
*/

const {
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject} = require("./lib.js");


function main() {
    f()
}

function ResolvePromise() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('Resolved Promise!')
        }, 4000);
    });
}

async function f(){
    console.log('calling Async function to resolve promise.')
    const result = await ResolvePromise();
    console.log(result);
}

main();