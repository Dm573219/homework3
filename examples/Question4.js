const {
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject} = require("./lib.js");

    async function withoutAwait(){
        let jsonFile = new Promise((resolve, reject) => {
            resolve("The Promise.");
        })
    let awaitTime = await 5;
    console.log("Running The Function.");
    console.log(jsonFile);
    console.log(awaitTime);
    console.log("Finished Await.") 
};

   