// This will work to show how Await cannot work without async, this will be a syntax error because Await needs Async function.


const {
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject} = require("./lib.js");

    function main(){
        GetTime();
    }
    function GetTime(location){
        const Clock = await fetchTime(location);
        return Clock;
    }
    

     
    
    main();