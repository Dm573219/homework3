// This will work to show how Async can work without Await.


const {
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject} = require("./lib.js");


    function main() {
        f()
    }
    
    const getPromiseReturned = async (Result) => {    
        return new Promise((resolve, reject) => {
          setTimeout(() => resolve(Result), 3500);
        });
     }
     (async() => {
         getPromiseReturned("Returning promise.")
         .then(result => {
             console.log('then -> ', result);
         })
         .catch(err => {
         console.log(err);
         });
     })();
     
    
    main();