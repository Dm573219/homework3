// 3. What happens when a promise object that returns is rejected?
//When a promise object is returned as rejected, then the handlers queued up with the promise's then methods. 
// These methods return a generated new promise. This is a chaining of methods. 

const { add1,
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject } = require("./lib.js");


function main() {
    async_reject("This method returns a promise, and will reject")
        .then(() => console.log("this will not be called"))
        .then(() => console.log("this will not be called either"))
        .catch((e) => console.log("SHOULD SEE: This catch will be called"))
        .then(() => console.log("SHOULD SEE: This then WILL run because the reject has been handled"))
        .catch((e) => console.log("This catch will NOT be called because the reject was handled above and no further rejects occured."))
}

main();