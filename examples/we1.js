// 1. Explain exception handling.
//Exception handling is basically a way in which a programmer can catch and raise or ignore input values or returned 
//Values that are detrimental to the code or run time in which it will not affect run time if these certain values come up
// and are handled. This will help the program to continue running without crashing when these events happen. 

const { add1,
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject } = require("./lib.js");


function main() {
    try {
        // foo calls bar, which raises an exception.
        // foo does not handle the exception with a try-catch,
        // so the exception continues up the call stack until it
        // gets here, where it is caught in the catch statement below.
        foo();
        console.log(
            "This is never printed because the exception raised in foo will skip the rest of the try block."
        );
    } catch (e) {
        console.log(e);
    }
}

function foo() {
    bar();
}

function bar() {
    throw {
        "error": "example",
        "message": "This demonstrates bar raising and exception"
    };
    console.log(
        "This never prints because the raised exception will skip the rest of the function."
    );
}

main();