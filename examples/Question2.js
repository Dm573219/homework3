const {
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject } = require("./lib.js");

function main(){
    async_reject("Returning promise, however Rejection will result.")
    .then(() => console.log("Will not be called."))
    .then(() => console.log("This will not be called neither."))
    .catch(() => console.log("This catches nothing, however it will be returned to console."))
    .catch((any) => console.log("Runs due to the rejection and this catch will be called. "))
    .then((any) => console.log("This will run because of the Rejection."))
}